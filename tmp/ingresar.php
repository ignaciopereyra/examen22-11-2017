<?php
	//Á
	include_once("general.inc.php");
	
	if ($_SESSION["usuario_logeado"] != "") {
		header("Location: /");
		die();
	}
	
	$meta_description = "Clasifiautos - Clasificados gratuitos de autos - Ingresar";
	$meta_keywords = "Clasificados gratis, autos nuevos y usados";
	$meta_robots = "all";
	$title = "Clasifiautos - Clasificados gratuitos de autos - Ingresar.";
	
	//Mensajes de error:
	$mensajes_error = array (
		_SITIO_ERR_MYSQL => "Error interno de MySQL",
		_SITIO_ERR_INGRESAR_USUARIO_VACIO => "No se ingresó el usuario",
		_SITIO_ERR_INGRESAR_USUARIO_MUY_LARGO => "El usuario no puede poseer más de 50 caracteres",
		_SITIO_ERR_INGRESAR_CONTRASENNA_VACIA => "No se ingresó la contraseña",
		_SITIO_ERR_INGRESAR_CONTRASENNA_MUY_LARGA => "La contraseña no puede poseer más de 50 caracteres",
		_SITIO_ERR_INGRESAR_USUARIO_CONTRASENNA_INVALIDOS => "El usuario y/o la contraseña, son inválidos"
	);
	
	$login_finalizado = false;
	if (isset($_POST["form_usuario"])) {
		foreach ($_POST as $nombre_campo => $valor) {
			$_POST[$nombre_campo] = stripslashes($valor);
		}
		$errores = login_usuario($_POST["form_usuario"], $_POST["form_contrasenna"], $mysql_conexion);
		if (!is_array($errores)) {
			$cod_usuario_logeado = $errores;
			$login_finalizado = true;
		}
	}
	else {
		$errores = array();
		$_POST = array (
			"form_usuario" => "",
			"form_contrasenna" => ""
		);
	}
	
	if (!$login_finalizado) {
		include_once("vista/header.inc.php");
		include_once("vista/ingresar.inc.php");
		include_once("vista/footer.inc.php");
	}
	else {
		$_SESSION["codigo_usuario_logeado"] = $cod_usuario_logeado;
		$_SESSION["usuario_logeado"] = $_POST["form_usuario"];
		header("Location: {$_SESSION["pagina_actual"]}");
		die();
	}
?>