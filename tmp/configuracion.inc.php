<?php
	//Á
	//Rutas:
	/*define("_SITIO_BARRA_SEPARADORA_RUTAS_SISTEMA_OPERATIVO", "/");
	define("_SITIO_RUTA_CONTROLADORES", "{RUTA_CARPETA_CONTROLADORES}");
	define("_SITIO_RUTA_FUNCIONES", "{RUTA_ARCHIVO_FUNCIONES}");
	define("_SITIO_RUTA_IMAGENES_AVISOS", "{RUTA_CARPETA_IMÁGENES_AVISOS}");
	define("_SITIO_RUTA_VISTA", "{RUTA_CARPETA_VISTAS}");*/
	
	//Comandos del sistema operativo:
	define("_SITIO_COMANDO_RMDIR", "RMDIR /S /Q [ruta]");
	
	//URLs y mails:
	define("_SITIO_URL_HOME", "/");
	define("_SITIO_URL_FICHA_AVISO", "/ficha_aviso.php?codigo=[codigo]&titulo=[titulo]");
	define("_SITIO_URL_IMAGEN_AVISO", "/imagenes/avisos");
	define("_SITIO_URL_IMAGEN_INEXISTENTE", "/vista/imagenes/sin_foto.png");
	define("_SITIO_URL_INGRESAR", "/ingresar.php");
	define("_SITIO_URL_PUBLICAR", "/publicar.php");
	define("_SITIO_URL_SALIR", "/salir.php");
	define("_SITIO_URL_VISTA", "/vista");
	define("_SITIO_URL_VISTA_IMAGENES", "/vista/imagenes");
	define("_SITIO_MAIL_NO_REPLY", "noreply@clasifiautos.com.ar");
	//define("_SITIO_DOMINIO_COOKIES_SITIO", ".clasifiautos.com.ar");
	
	//Varias:
	define("_SITIO_CHARSET", "UTF-8");
	
	//MySQL:
	define("_SITIO_MYSQL_CONEXION_HOST", "db");
	define("_SITIO_MYSQL_CONEXION_USUARIO", "root");
	define("_SITIO_MYSQL_CONEXION_CONTRASENNA", "1234");
	define("_SITIO_MYSQL_CONEXION_BD", "clasifiautos_db");
	define("_SITIO_MYSQL_CONEXION_PUERTO", 3306);
	define("_SITIO_MYSQL_CONEXION_CHARSET", "utf8");
	define("_SITIO_MYSQL_CONEXION_COLLATE", "utf8_spanish_ci");
?>