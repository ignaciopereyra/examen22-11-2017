<?php
	//Á
	function mysql_select(&$link_conexion, $query) {
		@mysqli_multi_query($link_conexion, $query);	//Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples.
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		$resultado = @mysqli_store_result($link_conexion); //Almacena el resultado.
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		
		//Vacía los resultados, para que la próxima llamada a un eventual CALL, no provoque el error 2014:
		while(@mysqli_next_result($link_conexion));
		
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		$cantidad = @mysqli_num_rows($resultado); //Cantidad de filas del resultset.
		if ($cantidad == 0) return null; //Se devuelve null, si no hay filas.
		$retorno = array(); //Se prepara el array de retorno.
		for ($x = 0; $x < $cantidad; $x++) { //Por todas las filas leídas...
			$retorno[$x] = @mysqli_fetch_assoc($resultado); //Cada registro irá como array asociativo.
			if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		}
		
		//El retorno será un array asociativo, de "n" filas numeradas, divididas en columnas cuyas claves coinciden con los nombres de los campos:
		return $retorno;
	}
	
	function mysql_insertar(&$link_conexion, $query) {
		//Intenta efectuar cualquier consulta INSERT.
		//Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
		@mysqli_multi_query($link_conexion, $query);
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		
		//Devuelve el LAST_INSERT_ID():
		return @mysqli_insert_id($link_conexion);
	}
	
	function mysql_eliminar(&$link_conexion, $query) {
		//Intenta efectuar cualquier consulta DELETE.
		//Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
		@mysqli_multi_query($link_conexion, $query);
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		
		//Devuelve el affected rows:
		return @mysqli_affected_rows($link_conexion);
	}
	
	function mysql_modificar(&$link_conexion, $query) {
		//Intenta efectuar cualquier consulta UPDATE.
		//Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
		@mysqli_multi_query($link_conexion, $query);
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		
		//Devuelve el affected rows:
		return @mysqli_affected_rows($link_conexion);
	}
	
	function mysql_sql(&$link_conexion, $query) {
		//Intenta efectuar cualquier consulta SQL, que no retribuya un resultset, un insert id o affected rows.
		//Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
		@mysqli_multi_query($link_conexion, $query);
		if (mysqli_errno($link_conexion)) return -mysqli_errno($link_conexion);	//Si hubo error, lo devuelve.
		
		//Devuelve 0, indicando que no hay error:
		return 0;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	function mail_valido($mail) {
		$mail = mb_strtolower(trim($mail));
		$longitud = mb_strlen($mail);
		if ($longitud < 3) return false;
		$posicion_arroba = mb_strpos($mail, "@");
		if ($posicion_arroba === false || $posicion_arroba === 0 || $posicion_arroba == $longitud - 1) return false;
		for ($parte = 0; $parte < 2; $parte++) {
			if ($parte == 0) {
				$fragmento = mb_substr($mail, 0, $posicion_arroba);
			}
			else {
				$fragmento = mb_substr($mail, $posicion_arroba + 1);
				if (mb_strpos($fragmento, ".") === false) return false;
			}
			$posicion_caracter = 0;
			$longitud_parte = mb_strlen($fragmento);
			$anterior = "";
			$longitud = mb_strlen($fragmento);
			for ($posicion = 0; $posicion < $longitud; $posicion++) {
				$caracter = $fragmento[$posicion];
				if (!(($caracter >= "a" && $caracter <= "z") || ($caracter >= "0" && $caracter <= "9") || $caracter == "_" || $caracter == "-" || $caracter == ".")) return false;
				if ($caracter == "." && ($anterior == "" || $anterior == "." || $posicion == $longitud - 1)) return false;
				$anterior = $caracter;
			}
		}
		
		return $mail;
	}
	
	function telefono_valido($telefono) {
		$caracteres_validos = "#()*+0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";
		$parentesis_abiertos = 0;
		$espera_digito = false;
		
		$telefono = str_replace(array(" ", "-", "/"), "", $telefono);
		$longitud = mb_strlen($telefono);
		if ($longitud == 0) return false;
		for ($posicion = 0; $posicion < $longitud; $posicion++) {
			$caracter = mb_substr($telefono, $posicion, 1);
			if ($caracter == "(") {
				$parentesis_abiertos++;
				$espera_digito = true;
			}
			else if ($caracter == ")") {
				if ($espera_digito) return false;
				$parentesis_abiertos--;
				if ($parentesis_abiertos < 0) return false;
				$espera_digito = false;
			}
			else {
				if (mb_strpos($caracteres_validos, $caracter) === false) return false;
				$espera_digito = false;
			}
		}
		if ($parentesis_abiertos > 0) return false;
		return true;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	function insertar_usuario_registrado($usuario, $contrasenna, $contrasenna_2, $mail, $telefono, $tipo_persona, &$link_mysql) {
		$errores = array();
		
		$usuario = trim($usuario);
		if (mb_strlen($usuario) < 1) $errores[] = _SITIO_ERR_USUARIOS_USUARIO_VACIO;
		if (mb_strlen($usuario) > 50) $errores[] = _SITIO_ERR_USUARIOS_USUARIO_MUY_LARGO;
		
		$contrasenna = trim($contrasenna);
		if (mb_strlen($contrasenna) < 1) $errores[] = _SITIO_ERR_USUARIOS_CONTRASENNA_VACIA;
		if (mb_strlen($contrasenna) > 50) $errores[] = _SITIO_ERR_USUARIOS_CONTRASENNA_MUY_LARGA;
		$contrasenna_2 = trim($contrasenna_2);
		if ($contrasenna != $contrasenna_2) $errores[] = _SITIO_ERR_USUARIOS_CONTRASENNAS_NO_COINCIDEN;
		
		$mail = trim($mail);
		if (mb_strlen($mail) < 1) $errores[] = _SITIO_ERR_USUARIOS_MAIL_VACIO;
		if (mb_strlen($usuario) > 255) $errores[] = _SITIO_ERR_USUARIOS_MAIL_MUY_LARGO;
		$mail = mail_valido($mail);
		if ($mail === false) $errores[] = _SITIO_ERR_USUARIOS_MAIL_INVALIDO;
		
		$telefono = trim($telefono);
		if ($telefono != "") {
			if (mb_strlen($usuario) > 40) $errores[] = _SITIO_ERR_USUARIOS_TELEFONO_MUY_LARGO;
			if (!telefono_valido($telefono)) $errores[] = _SITIO_ERR_USUARIOS_TELEFONO_INVALIDO;
		}
		
		if (count($errores) == 0) {
			$usuario = mysqli_real_escape_string($link_mysql, $usuario);
			$contrasenna = mysqli_real_escape_string($link_mysql, $contrasenna);
			$mail = mysqli_real_escape_string($link_mysql, $mail);
			$telefono = ($telefono == "" ? "null" : "'" . mysqli_real_escape_string($link_mysql, $telefono) . "'");
			$tipo_persona = mysqli_real_escape_string($link_mysql, $tipo_persona);
			
			$query = "INSERT INTO usuarios (usuario,contrasenna,mail,telefono,tipo_persona) VALUES ('$usuario', '$contrasenna', '$mail', $telefono, $tipo_persona)";
			$resultado = @mysql_insertar($link_mysql, $query);
			if ($resultado < 0) {
				if ($resultado == -1062) {
					$errores[] = _SITIO_ERR_USUARIOS_USUARIO_REPETIDO;
				}
				else {
					$errores[] = _SITIO_ERR_MYSQL;
				}
				
				return $errores;
			}
			else {
				return $resultado;
			}
		}
		else {
			return $errores;
		}
	}
	
	function recuperar_contrasenna($mail, &$link_mysql) {
		$errores = array();
		$mail = trim($mail);
		if (mb_strlen($mail) < 1) $errores[] = _SITIO_ERR_RECUPERAR_CLAVE_MAIL_VACIO;
		if (mb_strlen($usuario) > 255) $errores[] = _SITIO_ERR_RECUPERAR_CLAVE_MAIL_MUY_LARGO;
		$mail = mail_valido($mail);
		if ($mail === false) $errores[] = _SITIO_ERR_RECUPERAR_CLAVE_MAIL_INVALIDO;
		if (count($errores) == 0) {
			$mail = mysqli_real_escape_string($link_mysql, $mail);
			$nueva_contrasenna = mt_rand(100000, 999999);
			$query = "UPDATE usuarios SET contrasenna=$nueva_contrasenna WHERE mail='$mail' AND activo=1";
			$resultado = @mysql_modificar($link_mysql, $query);
			if ($resultado == 0) {
				$errores[] = _SITIO_ERR_RECUPERAR_CLAVE_USUARIO_INEXISTENTE;
				return $errores;
			}
			else {
				@mail($mail, "Nueva contraseña - Clasifiautos", "Estimado usuario: su nueva contraseña es <b>$nueva_contrasenna</b>", "From: " . _SITIO_MAIL_NO_REPLY . "\r\nContent-Type: text/html; charset=utf-8");
				return true;
			}
		}
		else {
			return $errores;
		}
	}
	
	function login_usuario($usuario, $contrasenna, &$link_mysql) {
		$errores = array();
		
		$usuario = trim($usuario);
		if (mb_strlen($usuario) < 1) $errores[] = _SITIO_ERR_INGRESAR_USUARIO_VACIO;
		if (mb_strlen($usuario) > 50) $errores[] = _SITIO_ERR_INGRESAR_USUARIO_MUY_LARGO;
		
		$contrasenna = trim($contrasenna);
		if (mb_strlen($contrasenna) < 1) $errores[] = _SITIO_ERR_INGRESAR_CONTRASENNA_VACIA;
		if (mb_strlen($contrasenna) > 50) $errores[] = _SITIO_ERR_INGRESAR_CONTRASENNA_MUY_LARGA;
		
		if (count($errores) == 0) {
			$usuario = mysqli_real_escape_string($link_mysql, $usuario);
			$contrasenna = mysqli_real_escape_string($link_mysql, $contrasenna);
			$query = "SELECT codigo FROM usuarios WHERE usuario='$usuario' AND contrasenna='$contrasenna' AND activo=1";
			$resultado = mysql_select($link_mysql, $query);
			if (!isset($resultado[0]["codigo"])) {
				$errores[] = _SITIO_ERR_INGRESAR_USUARIO_CONTRASENNA_INVALIDOS;
				return $errores;
			}
			else {
				return $resultado[0]["codigo"];
			}
		}
		else {
			return $errores;
		}
	}
	
	function imagen_valida($archivo, $tipo_mime) {
		switch ($tipo_mime) {
			case "image/jpeg":
				$imagen = @imagecreatefromjpeg($archivo);
				break;
			case "image/png":
				$imagen = @imagecreatefrompng($archivo);
				break;
			case "image/gif":
				$imagen = @imagecreatefromgif($archivo);
				break;
		}
		return ($imagen === false ? false : true);
	}
	
	function borrar_directorio_completo ($directorio) {
		@shell_exec(str_replace("[ruta]", str_replace("/", _SITIO_BARRA_SEPARADORA_RUTAS_SISTEMA_OPERATIVO, $directorio), _SITIO_COMANDO_RMDIR));
	}
	
	function adaptar_tamanno_foto($archivo_original, $tipo_mime, $ancho_miniatura, $alto_miniatura, $ruta_destino) {
		switch ($tipo_mime) {
			case "image/jpeg":
				$original = @imagecreatefromjpeg($archivo_original);
				break;
			case "image/gif":
				$original = @imagecreatefromgif($archivo_original);
				break;
			case "image/png":
				$original = @imagecreatefrompng($archivo_original);
				break;
			default:
				return false;
		}
		if ($original === false) return false;
		$ancho_original = imagesx($original);
		$alto_original = imagesy($original);
		
		//Dimensionamiento:
		if ($alto_miniatura > $alto_original && $ancho_miniatura > $ancho_original) {
			$alto_miniatura = $alto_original;
			$ancho_miniatura = $ancho_original;
		}
		else {
			$factor = $alto_miniatura / $alto_original;
			if (floor($ancho_original * $factor) > $ancho_miniatura) {
				$factor = $ancho_miniatura / $ancho_original;
			}
			$ancho_miniatura = floor($ancho_original * $factor);
			$alto_miniatura = floor($alto_original * $factor);
			if ($ancho_miniatura == 0) $ancho_miniatura = 1;
			if ($alto_miniatura == 0) $alto_miniatura = 1;
		}
		$thumb = @imagecreatetruecolor($ancho_miniatura, $alto_miniatura);
		@imagecopyresampled($thumb, $original, 0, 0, 0, 0, $ancho_miniatura, $alto_miniatura, $ancho_original, $alto_original);
		switch ($tipo_mime) {
			case "image/jpeg":
				imagejpeg($thumb, $ruta_destino);
				break;
			case "image/gif":
				imagegif($thumb, $ruta_destino);
				break;
			case "image/png":
				imagepng($thumb, $ruta_destino);
				break;
		}
		
		return true;
	}
	
	function generar_url_simplificada($texto) {
		//ESPACIO.
		$array_equivalencias = array (
			"-" => array(" ", "!", "\"", "#", "\$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~", "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", "«", "¬", "­", "®", "¯", "°", "±", "²", "³", "´", "µ", "¶", "•", "¸", "¹", "º", "»", "¼", "½", "¾", "¿", "×", "÷"),
			"a" => array("À", "Á", "Â", "Ã", "Ä", "Å", "à", "á", "â", "ã", "ä", "å", "Ā", "ā", "Ă", "ă", "Ą", "ą"),
			"ae" => array("Æ", "æ"),
			"b" => array("ƀ", "Ɓ", "Ƃ", "ƃ", "Ƅ", "ƅ"),
			"c" => array("Ç", "ç", "Ć", "ć", "Ĉ", "ĉ", "Ċ", "ċ", "Č", "č", "Ƈ", "ƈ"),
			"d" => array("Ð", "ð", "Ď", "ď", "Đ", "đ", "Ɖ", "Ɗ", "Ƌ", "ƌ", "ƍ"),
			"e" => array("È", "É", "Ê", "Ë", "è", "é", "ê", "ë", "Ē", "ē", "Ĕ", "ĕ", "Ė", "ė", "Ę", "ę", "Ě", "ě", "Ǝ", "Ə", "Ɛ"),
			"f" => array("Ƒ", "ƒ"),
			"g" => array("Ĝ", "ĝ", "Ğ", "ğ", "Ġ", "ġ", "Ģ", "ģ", "Ɠ", "Ɣ"),
			"h" => array("Ĥ", "ĥ", "Ħ", "ħ"),
			"i" => array("Ì", "Í", "Î", "Ï", "ì", "í", "î", "ï", "Ĩ", "ĩ", "Ī", "ī", "Ĭ", "ĭ", "Į", "į", "İ", "ı", "Ɩ", "Ɨ"),
			"ij" => array("Ĳ", "ĳ"),
			"j" => array("Ĵ", "ĵ"),
			"jua" => array("ƕ"),
			"k" => array("Ķ", "ķ", "ĸ", "Ƙ", "ƙ"),
			"l" => array("Ĺ", "ĺ", "Ļ", "ļ", "Ľ", "ľ", "Ŀ", "ŀ", "Ł", "ł", "ƚ", "ƛ"),
			"n" => array("Ñ", "ñ", "Ń", "ń", "Ņ", "ņ", "Ň", "ň", "ŉ", "Ɲ", "ƞ"),
			"ng" => array("Ŋ", "ŋ"),
			"o" => array("Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "ò", "ó", "ô", "õ", "ö", "ø", "Ō", "ō", "Ŏ", "ŏ", "Ő", "ő", "Ɔ", "Ɵ", "Ơ", "ơ", "Ʊ"),
			"oe" => array("Œ", "œ"),
			"p" => array("Ƥ", "ƥ"),
			"q" => array("Ƣ", "ƣ"),
			"r" => array("Ŕ", "ŕ", "Ŗ", "ŗ", "Ř", "ř", "Ʀ"),
			"s" => array("Ś", "ś", "Ŝ", "ŝ", "Ş", "ş", "Š", "š", "ſ", "Ƨ", "ƨ", "Ʃ", "ƪ"),
			"ss" => array("ß"),
			"t" => array("Ţ", "ţ", "Ť", "ť", "Ŧ", "ŧ", "ƫ", "Ƭ", "ƭ", "Ʈ"),
			"u" => array("Ù", "Ú", "Û", "Ü", "ù", "ú", "û", "ü", "Ũ", "ũ", "Ū", "ū", "Ŭ", "ŭ", "Ů", "ů", "Ű", "ű", "Ų", "ų", "Ư", "ư"),
			"v" => array("Ʋ"),
			"w" => array("Ŵ", "ŵ", "Ɯ"),
			"y" => array("Ý", "ý", "ÿ", "Ŷ", "ŷ", "Ÿ", "Ƴ", "ƴ"),
			"z" => array("Þ", "þ", "Ź", "ź", "Ż", "ż", "Ž", "ž", "Ƶ", "ƶ")
		);
		$resultado = $texto;
		foreach ($array_equivalencias as $letra_latina => $letras_varias) {
			$resultado = str_replace($letras_varias, $letra_latina, $resultado);
		}
		$resultado = mb_strtolower($resultado);
		return urlencode($resultado);
	}
	
	function extension_archivo($archivo) {
		$posicion = mb_strripos($archivo, ".");
		if ($posicion === false) return false;
		$extension = mb_strtolower(trim(mb_substr($archivo, $posicion + 1)));
		
		return $extension;
	}
	
	function obtener_tipo_mime_imagen($archivo) {
		$extension = extension_archivo($archivo);
		switch ($extension) {
			case "gif":
				return "image/gif";
			case "jpg":
			case "jpeg":
				return "image/jpeg";
			case "png":
				return "image/png";
			default:
				return false;
		}
	}
	
	function insertar_aviso($titulo, $duracion, $direccion, $localidad, $descripcion, $mostrar_mail, $oferta, $modelo, $anno, $combustible, $kilometraje, $estado, $precio, $moneda, $destacar, $fotos, &$link_mysql) {
		$errores = array();
		
		$titulo = trim($titulo);
		if (mb_strlen($titulo) < 1) $errores[] = _SITIO_ERR_PUBLICAR_TITULO_VACIO;
		if (mb_strlen($titulo) > 200) $errores[] = _SITIO_ERR_PUBLICAR_TITULO_MUY_LARGO;
		
		$duracion = (int)$duracion;
		$duraciones_validas = array(30, 60, 90, 0);
		if (array_search($duracion, $duraciones_validas) === false) $errores[] = _SITIO_ERR_PUBLICAR_DURACION_INVALIDA;
		
		$direccion = trim($direccion);
		if (mb_strlen($direccion) > 70) $errores[] = _SITIO_ERR_PUBLICAR_DIRECCION_MUY_LARGO;
		
		$descripcion = trim($descripcion);
		if (mb_strlen($descripcion) < 1) $errores[] = _SITIO_ERR_PUBLICAR_DESCRIPCION_VACIA;
		if (mb_strlen($descripcion) > 65535) $errores[] = _SITIO_ERR_PUBLICAR_DESCRIPCION_MUY_LARGA;
		
		$anno = (int)$anno;
		if ($anno < 1900 || $anno > date("Y", time())) $errores[] = _SITIO_ERR_PUBLICAR_ANNO_INVALIDO;
		
		if (mb_strlen($kilometraje) < 1) $errores[] = _SITIO_ERR_PUBLICAR_KILOMETRAJE_VACIO;
		if (mb_strlen($kilometraje) > 13) $errores[] = _SITIO_ERR_PUBLICAR_KILOMETRAJE_MUY_LARGO;
		$kilometraje = (double)$kilometraje;
		if ($kilometraje < 0) $errores[] = _SITIO_ERR_PUBLICAR_KILOMETRAJE_INVALIDO;
		
		if (mb_strlen($precio) < 1) $errores[] = _SITIO_ERR_PUBLICAR_PRECIO_VACIO;
		if (mb_strlen($precio) > 17) $errores[] = _SITIO_ERR_PUBLICAR_PRECIO_MUY_LARGO;
		$precio = (double)$precio;
		if ($precio <= 0) $errores[] = _SITIO_ERR_PUBLICAR_PRECIO_INVALIDO;
		
		if (count($fotos) > 0) {
			foreach ($fotos as $foto) {
				if (!isset($foto["error"])) {
					$errores[] = _SITIO_ERR_PUBLICAR_FOTO_ARCHIVO_CORRUPTO;
					break;
				}
				switch ($foto["error"]) {
					case UPLOAD_ERR_OK:	//No hubo error en la subida del archivo.
						$mime = obtener_tipo_mime_imagen($foto["name"]);
						switch ($mime) {
							case "image/jpeg":
							case "image/png":
							case "image/gif":
								if (!imagen_valida($foto["tmp_name"], $mime)) {
									$errores[] = _SITIO_ERR_PUBLICAR_FOTO_ARCHIVO_CORRUPTO;
									break 3;
								}
								else {
									break 2;
								}
							default:
								$errores[] = _SITIO_ERR_PUBLICAR_FOTO_EXTENSION_INVALIDA;
								break 3;
						}
					case UPLOAD_ERR_NO_FILE:	//No se eligió ningún archivo.
						break;
					case UPLOAD_ERR_INI_SIZE: //Se excedió el tamaño estipulado por la directiva upload_max_filesize, de php.ini.
					case UPLOAD_ERR_FORM_SIZE: //Se excedió el tamaño estipulado por el INPUT HIDDEN "MAX_FILE_SIZE".
						$errores[] = _SITIO_ERR_PUBLICAR_FOTO_TAMANNO_EXCESIVO;
						break 2;
					case UPLOAD_ERR_PARTIAL: //El archivo sólo se subió parcialmente.
					case UPLOAD_ERR_NO_TMP_DIR: //Pérdida de la carpeta temporal.
					case UPLOAD_ERR_CANT_WRITE: //No se pudo escribir en disco.
						$errores[] = _SITIO_ERR_PUBLICAR_FOTO_ERROR_INTERNO;
						break 2;
					default:
						break 2;
				}
			}
		}
		
		if (count($errores) == 0) {
			$titulo = mysqli_real_escape_string($link_mysql, $titulo);
			if ($direccion == "") {
				$direccion = "null";
			}
			else {
				$direccion = "'" . mysqli_real_escape_string($link_mysql, $direccion) . "'";
			}
			$localidad = (int)$localidad;
			$descripcion = mysqli_real_escape_string($link_mysql, $descripcion);
			$mostrar_mail = (int)$mostrar_mail;
			if ($mostrar_mail != 0) $mostrar_mail = 1;
			$oferta = (int)$oferta;
			if ($oferta != 0) $oferta = 1;
			$modelo = (int)$modelo;
			$combustible = (int)$combustible;
			$estado = (int)$estado;
			$moneda = (int)$moneda;
			$destacar = (int)$destacar;
			if ($destacar != 0) $destacar = 1;
			
			$query = "INSERT INTO avisos (
						usuario,
						fecha_hora_inicio,
						duracion,
						direccion,
						localidad,
						titulo,
						descripcion,
						mostrar_mail,
						oferta,
						modelo_auto,
						anno_auto,
						combustible,
						kilometraje,
						estado_auto,
						precio,
						moneda,
						destacar,
						visitas,
						marcas_spam,
						revisado,
						estado_aviso
					)
					VALUES (
						{$_SESSION["codigo_usuario_logeado"]},
						NOW(),
						$duracion,
						$direccion,
						$localidad,
						'$titulo',
						'$descripcion',
						$mostrar_mail,
						$oferta,
						$modelo,
						$anno,
						$combustible,
						$kilometraje,
						$estado,
						$precio,
						$moneda,
						$destacar,
						0,
						0,
						0,
						1
					)";
			
			$resultado = @mysql_insertar($link_mysql, $query);
			
			if ($resultado < 0) {
				$errores[] = _SITIO_ERR_MYSQL;
				return $errores;
			}
			else {
				//Inserción de fotos:
				$codigo_aviso = $resultado;
				if (count($fotos) > 0) {
					$conteo_foto = 1;
					
					//Crea la carpeta, en la que se almacenarán las fotos:
					$ruta_fotos_aviso = _SITIO_RUTA_IMAGENES_AVISOS . "/$codigo_aviso";
					$ruta_fotos_aviso_imagenes = _SITIO_RUTA_IMAGENES_AVISOS . "/$codigo_aviso/imagenes";
					$ruta_fotos_aviso_miniaturas = _SITIO_RUTA_IMAGENES_AVISOS . "/$codigo_aviso/miniaturas";
					@mkdir($ruta_fotos_aviso);
					@mkdir($ruta_fotos_aviso_imagenes);
					@mkdir($ruta_fotos_aviso_miniaturas);
					foreach ($fotos as $foto) {
						if ($foto["error"] == UPLOAD_ERR_NO_FILE) continue;
						$mime = obtener_tipo_mime_imagen($foto["name"]);
						switch ($mime) {
							case "image/jpeg":
								$extension = "jpg";
								break;
							case "image/png":
								$extension = "png";
								break;
							case "image/gif":
								$extension = "gif";
								break;
						}
						$query = "INSERT INTO avisos_fotos (
									aviso,
									foto,
									miniatura,
									orden
								)
								VALUES (
									$codigo_aviso,
									'$conteo_foto.$extension',
									'$conteo_foto.$extension',
									$conteo_foto
								)";
						$resultado = @mysql_insertar($link_mysql, $query);
						if ($resultado < 0) {
							$errores[] = _SITIO_ERR_MYSQL;
							@mysql_eliminar($link_mysql, "DELETE FROM avisos WHERE codigo=$codigo_aviso");
							@borrar_directorio_completo($ruta_fotos_aviso);
							return $errores;
						}
						else {
							$ruta_final_archivo_temporal = "$ruta_fotos_aviso_imagenes/$conteo_foto" . "_dup.$extension";
							$movimiento = @move_uploaded_file($foto["tmp_name"], $ruta_final_archivo_temporal);
							if (!$movimiento) {
								$errores[] = _SITIO_ERR_PUBLICAR_FOTO_ERROR_INTERNO;
								@mysql_eliminar($link_mysql, "DELETE FROM avisos WHERE codigo=$codigo_aviso");
								@borrar_directorio_completo($ruta_fotos_aviso);
								return $errores;
							}
							
							//Trata de adaptar la imagen:
							$ruta_final_archivo = "$ruta_fotos_aviso_imagenes/$conteo_foto.$extension";
							$imagen_adaptada = adaptar_tamanno_foto($ruta_final_archivo_temporal, $mime, _SITIO_FOTO_MAX_ANCHO, _SITIO_FOTO_MAX_ALTO, $ruta_final_archivo);
							@unlink($ruta_final_archivo_temporal);
							
							//Genera la miniatura:
							$ruta_final_miniatura = "$ruta_fotos_aviso_miniaturas/$conteo_foto.$extension";
							$miniatura_generada = adaptar_tamanno_foto($ruta_final_archivo, $mime, _SITIO_MINIATURA_MAX_ANCHO, _SITIO_MINIATURA_MAX_ALTO, $ruta_final_miniatura);
							if (!$miniatura_generada) {
								$errores[] = _SITIO_ERR_PUBLICAR_FOTO_ERROR_INTERNO;
								@mysql_eliminar($link_mysql, "DELETE FROM avisos WHERE codigo=$codigo_aviso");
								@borrar_directorio_completo($ruta_fotos_aviso);
								return $errores;
							}
						}
						$conteo_foto++;
					}
					if ($conteo_foto == 1) @borrar_directorio_completo($ruta_fotos_aviso);
					
					//Devuelve un array, con el código del aviso y su título:
					return array (
						"codigo_aviso" => $codigo_aviso,
						"titulo_aviso" => $titulo
					);
				}
				else {
					return $codigo_aviso;
				}
			}
		}
		else {
			return $errores;
		}
	}
	
	function acortar_texto($texto, $longitud_maxima) {
		$puntuaciones = " !\"'(),-./:;?[\\]`{|}¡«­´»¿";
		
		$longitud = mb_strlen($texto);
		if ($longitud <= $longitud_maxima) return $texto;
		$texto = mb_substr($texto, 0, $longitud_maxima + 1);
		for ($x = $longitud_maxima; $x >= 0; $x--) {
			$caracter = mb_substr($texto, $x, 1);
			if (mb_stripos($puntuaciones, $caracter) !== false) {
				return mb_substr($texto, 0, $x);
			}
		}
		
		return mb_substr($texto, 0, $longitud_maxima);
	}
?>