<?php
	//Á
	include_once("general.inc.php");
	
	if (!isset($_GET["codigo"])) {
		include_once("404.php");
		die();
	}
	else {
		$_GET["codigo"] = (int)$_GET["codigo"];
	}
	
	//Intenta leer el aviso:
	@mysql_sql($mysql_conexion, "SET lc_time_names='es_AR'");
	$aviso = @mysql_select($mysql_conexion, "SELECT avisos.codigo AS aviso_codigo,DATE_FORMAT(avisos.fecha_hora_inicio, '%e de %M de %Y') AS aviso_fecha,avisos.direccion AS aviso_direccion,avisos.titulo AS aviso_titulo,avisos.descripcion AS aviso_descripcion,avisos.mostrar_mail AS aviso_mostrar_mail,avisos.oferta AS aviso_oferta,avisos.anno_auto AS aviso_anno_auto,avisos.kilometraje AS aviso_kilometraje,avisos.visitas AS aviso_visitas,
												usuarios.usuario AS usuario_nombre,usuarios.mail AS usuario_mail,usuarios.telefono AS usuario_telefono,
												localidades.localidad AS localidad_nombre,
												autos_modelos.modelo AS modelo_nombre,
												tipos_combustibles.tipo AS combustible_tipo,
												autos_estados.estado AS estado_auto,
												replace(monedas.plantilla,'[v]',avisos.precio) AS aviso_precio,
												provincias.provincia AS provincia_nombre,
												autos_marcas.marca AS marca_nombre,
												avisos_fotos.foto AS aviso_foto
											FROM avisos
												INNER JOIN usuarios ON avisos.usuario=usuarios.codigo
												INNER JOIN localidades ON avisos.localidad=localidades.codigo
												INNER JOIN autos_modelos ON avisos.modelo_auto=autos_modelos.codigo
												INNER JOIN tipos_combustibles ON avisos.combustible=tipos_combustibles.codigo
												INNER JOIN autos_estados ON avisos.estado_auto=autos_estados.codigo
												INNER JOIN monedas ON avisos.moneda=monedas.codigo
												INNER JOIN avisos_estados ON avisos.estado_aviso=avisos_estados.codigo
												INNER JOIN provincias ON localidades.provincia=provincias.codigo
												INNER JOIN autos_marcas ON autos_modelos.marca=autos_marcas.codigo
												LEFT OUTER JOIN avisos_fotos ON avisos.codigo=avisos_fotos.aviso
											WHERE avisos.codigo={$_GET["codigo"]}
												AND (duracion=0 OR ADDDATE(fecha_hora_inicio,duracion)>NOW())
												AND estado_aviso=1
												AND usuarios.activo=1
											ORDER BY avisos_fotos.orden ASC");
	
	if (!isset($aviso[0]["aviso_codigo"])) {
		include_once("404.php");
		die();
	}
	
	//Aumenta el número de visitas:
	//@mysql_modificar($mysql_conexion, "UPDATE avisos SET visitas=visitas+1 WHERE codigo={$aviso[0]["aviso_codigo"]}");
	
	$meta_description = "Clasifiautos - Clasificados gratuitos de autos - {$aviso[0]["aviso_titulo"]}";
	$meta_keywords = "Clasificados gratis, autos nuevos y usados";
	$meta_robots = "all";
	$title = "Clasifiautos - Clasificados gratuitos de autos. {$aviso[0]["aviso_titulo"]}.";
	
	include_once("vista/header.inc.php");
	include_once("vista/ficha_aviso.inc.php");
	include_once("vista/footer.inc.php");
?>