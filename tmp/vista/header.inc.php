<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- Á -->
		<meta charset="utf-8" />
		
		<meta name="application-name" content="Clasifiautos" />
		<meta name="author" content="Pablo Esteban Mendez Domeneche y alumnos" />
		<meta name="description" content="<?= htmlspecialchars($meta_description, ENT_COMPAT, _SITIO_CHARSET) ?>" />
		<meta name="designer" content="Pablo Esteban Mendez Domeneche y alumnos" />
		<meta name="generator" content="Pablo Esteban Mendez Domeneche y alumnos" />
		<meta name="googlebot" content="<?= htmlspecialchars($meta_robots, ENT_COMPAT, _SITIO_CHARSET) ?>" />
		<meta name="keywords" content="<?= htmlspecialchars($meta_keywords, ENT_COMPAT, _SITIO_CHARSET) ?>" />
		<meta name="publisher" content="http://www.redprogramar.com.ar" />
		<meta name="rating" content="General" />
		<meta name="revisit-after" content="2 days" />
		<meta name="robots" content="<?= htmlspecialchars($meta_robots, ENT_COMPAT, _SITIO_CHARSET) ?>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title><?= htmlspecialchars($title, ENT_COMPAT, _SITIO_CHARSET) ?></title>
		
		<link rel="stylesheet" type="text/css" href="<?= _SITIO_URL_VISTA ?>/estilos.css" hreflang="es" media="screen" />
	</head>
	<body>
		<div class="principal">
			<div class="header">
				<div class="header_logo">
					<a href="<?= _SITIO_URL_HOME ?>" hreflang="es" media="screen and (min-width:1024px)" rel="home section start" type="text/html" draggable="false"><img class="header_logo" alt="Clasifiautos" src="<?= _SITIO_URL_VISTA_IMAGENES ?>/logo_header.jpg" draggable="false" /></a>
				</div>
				<div class="header_titulo_opciones">
					<div class="header_titulo">
						<p class="header_titulo">
							<span class="header_titulo">Clasifiautos</span>
						</p>
					</div>
					<div class="header_opciones">
						<?php
							if ($_SESSION["usuario_logeado"] != "") {
						?>
								<div class="header_opciones_opcion_usuario_logeado">
									<p class="header_opciones_opcion_usuario_logeado">
										<span class="header_opciones_opcion_usuario_logeado">Hola <?= htmlspecialchars($_SESSION["usuario_logeado"], ENT_COMPAT, _SITIO_CHARSET) ?></span>
									</p>
								</div>
								<div class="header_opciones_opcion"><a class="header_opciones_opcion" href="<?= _SITIO_URL_SALIR ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html">Salir</a></div>
						<?php
							}
							else {
						?>
								<div class="header_opciones_opcion"><a class="header_opciones_opcion" href="<?= _SITIO_URL_INGRESAR ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html">Ingresar</a></div>
						<?php
							}
						?>
					</div>
				</div>
			</div>