<?php
	//Á
	include_once("general.inc.php");
	
	$meta_description = "Clasifiautos - Clasificados gratuitos de autos";
	$meta_keywords = "Clasificados gratis, autos nuevos y usados";
	$meta_robots = "all";
	$title = "Clasifiautos - Clasificados gratuitos de autos.";
	
	if (!isset($_GET["pagina"])) {
		$pagina = 1;
	}
	else {
		$pagina = $_GET["pagina"];
	}
	
	$desde = 20 * ($pagina - 1);
	@mysql_sql($mysql_conexion, "SET lc_time_names='es_AR'");
	$avisos = @mysql_select($mysql_conexion, "SELECT avisos_fotos.miniatura,
														avisos.codigo,
														avisos.titulo,
														DATE_FORMAT(avisos.fecha_hora_inicio, '%e de %M de %Y') AS fecha,
														avisos.descripcion,
														avisos.destacar 
												FROM avisos 
													LEFT OUTER JOIN avisos_fotos ON avisos.codigo=avisos_fotos.aviso 
													INNER JOIN usuarios ON avisos.usuario=usuarios.codigo 
												WHERE 
													(avisos.duracion=0 OR ADDDATE(avisos.fecha_hora_inicio,avisos.duracion)>NOW()) 
													AND avisos.estado_aviso=1 
													AND (ISNULL(avisos_fotos.orden) OR avisos_fotos.orden=1) 
													AND usuarios.activo=1 
												ORDER BY avisos.destacar DESC,avisos.fecha_hora_inicio DESC 
												LIMIT $desde," . 20);
	
	//Calcula el total de páginas:
	$total_avisos = @mysql_select($mysql_conexion, "SELECT COUNT(avisos.codigo) AS total 
													FROM avisos 
														INNER JOIN usuarios ON avisos.usuario=usuarios.codigo 
													WHERE 
														(avisos.duracion=0 OR ADDDATE(avisos.fecha_hora_inicio,avisos.duracion)>NOW()) 
														AND avisos.estado_aviso=1 
														AND usuarios.activo=1");
	
	$total_avisos = $total_avisos[0]["total"];
	$total_paginas = ceil($total_avisos / 20);
	
	include_once("vista/header.inc.php");
	include_once("vista/index.inc.php");
	include_once("vista/footer.inc.php");
?>