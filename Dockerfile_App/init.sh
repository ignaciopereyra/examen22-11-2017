#!/bin/bash

rm -f /var/www/html/configuracion.inc.php
cp /var/www/html/configuracion.inc.php.template /var/www/html/configuracion.inc.php
sed -i "s/{host}/$MYSQL_DB_HOST/g" /var/www/html/configuracion.inc.php
sed -i "s/{user}/$MYSQL_DB_USER/g" /var/www/html/configuracion.inc.php
sed -i "s/{pass}/$MYSQL_DB_PASSWORD/g" /var/www/html/configuracion.inc.php
sed -i "s/{db}/$MYSQL_DB_DATABASE/g" /var/www/html/configuracion.inc.php

apache2-foreground
